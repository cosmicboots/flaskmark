from flask import Flask, render_template, url_for, request, redirect, session
import time
import sqlite3

app = Flask(__name__)

app.secret_key = "super_secret_thing1"

@app.route("/", methods=["POST","GET"])
def home():
    db = Controller("main.db")
    data = db.getColumns("bookmarks", ["bookmark", "url", "id"])

    if not session.get("edit"):
        session["edit"] = False

    if request.method == "POST":
        reqDict = dict(request.form)
        if "editToggle" in reqDict:
            session["edit"] = not session.get("edit")

    return(render_template('home.html', bookmarks=data, edit=session.get("edit")))

@app.route("/bmhandler", methods=["POST"])
def bmHandler():
    db = Controller("main.db")
    if request.method == "POST":
        reqDict = dict(request.form)
        print(reqDict)
        reqData = list(reqDict.items())
        print(reqData)
        if reqData[0][0][:3] == "del":
            db.delEntry("bookmarks", "id", reqData[0][0][4:])
        if reqData[0][0][:4] == "edit":
            print("Editing entry")
            for field in reqData:
                print(field)
                if field[0][5:9] == "name":
                    print(field[1][0])
                    editName = field[1][0]
                if field[0][5:8] == "url":
                    print(field[1][0])
                    editURL = field[1][0]
                if field[0][5:7] == "id":
                    print(field[0][8:])
                    editId = field[0][8:]
            db.editEntry("bookmarks", "id", "bookmark", editId, editName)
            db.editEntry("bookmarks", "id", "url", editId, editURL)
        if "new-bookmark" in reqDict:
            db.addEntry("bookmarks", ["admin", reqDict["new-name"][0], "http://" + reqDict["new-url"][0], str(time.time())])
    return(redirect(url_for("home")))

@app.route("/dbtest")
def test():
    db = Controller("main.db")
    db.createTable("bookmarks", "user", ["bookmark", "url", "id"])
    db.addEntry("bookmarks", ["admin", "google", "https://google.com", str(time.time())])
    db.addEntry("bookmarks", ["admin", "Duckduckgo", "https://duckduckgo.com", str(time.time())])
    x = db.getColumns("bookmarks", ["user", "bookmark"]) 
    db.close()
    return("<a href='/'>home</a>")

@app.route("/quickadd")
def quickadd():
    user = "admin"
    name = request.args.get("name")
    url = request.args.get("url")
    print(name)
    print(url)
    db = Controller("main.db")
    db.addEntry("bookmarks", [name, name, url, str(time.time())])
    return("<a href='/'>home</a>")

class Controller:
    def __init__(self, name):
        self.dbName = name
        self.conn = sqlite3.connect(self.dbName)
        self.c = self.conn.cursor()
    
    def createTable(self, table, first, columns):
        try:
            self.c.execute("CREATE TABLE IF NOT EXISTS {0} ({1} TEXT)".format(table, first))
            print("Creating table")
            [self.addColumn(table, column) for column in columns]
            print("Adding columns")
        except:
            pass

    def addColumn(self, table, column):
        self.c.execute("ALTER TABLE {0} ADD COLUMN {1} TEXT".format(table, column))

    def addEntry(self, table, entry):
        self.c.execute("INSERT INTO {0} VALUES ({1})".format(table, "'" + "', '".join(entry) + "'"))
        self.conn.commit()

    def editEntry(self, table, testCol, setCol, testVal, setVal):
        self.c.execute("UPDATE {0} SET {1} = '{2}' WHERE {3} IS '{4}'".format(table, setCol, setVal, testCol, testVal))
        self.conn.commit()

    def delEntry(self, table, matchColumn, entry):
        self.c.execute("DELETE FROM {0} WHERE {1} = '{2}'".format(table, matchColumn, entry))
        self.conn.commit()

    def getColumns(self, table, column):
        self.c.execute("SELECT {1} FROM {0}".format(table, ', '.join(column)))
        return(self.c.fetchall())

    def close(self):
        self.c.close()
        self.conn.close()

    def dropTable(self, table):
        self.c.execute("DROP TABLE {0}".format(table))


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)
